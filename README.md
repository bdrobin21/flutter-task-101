# Flutter Task 101

This is a practical task to participate in Brain Station 23

## UI
Currently it is based on 3 page. Home,Repository details and Login page
<p align="center"><img src="https://gitlab.com/bdrobin21/flutter-task-101/-/raw/main/image_for_readme/ui.png?raw=true" alt="Mockup"></p>

## Structure
App structure follow Repository pattern with MVC(Model-View-Controller) framework and GetX state management which have intelligent dependency injection.
<p align="center"><img src="https://gitlab.com/bdrobin21/flutter-task-101/-/raw/main/image_for_readme/diagram.png?raw=true" alt="Diagram"></p>


## Sketch
Sketch is so important for UI design
<p align="center"><img src="https://gitlab.com/bdrobin21/flutter-task-101/-/raw/main/image_for_readme/sketch.PNG?raw=true" alt="Sketch"></p>

## Some unfinished task
This is not main task as per requirement on question. It is used for adding more features.
* User list page
* User details page
* Login implementation

## Future scope
In future, by this project, user can login in their github account. They can search for project they want. They can share their project to other. They can find user used github. They can do all things that the rest api is available for the task.
For improve code quality we have to 
* Mention task comment. 
* Perform different types of testing. 
* Remove boilerplate code.
