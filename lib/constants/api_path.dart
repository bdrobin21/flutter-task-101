
class ApiPath{
  static const baseUrl = 'https://api.github.com/';

  static const pageNo = '&page=';
  static const perPage = '&per_page=100';


// These url not needed authentication
  static const userSearchUrl = 'search/users?q=flutter';
  static const userUrl = 'users/'; //Need to provide user name after slash
  static const topicSearchUrl = 'search/topics?q=flutter&page=1&per_page=10';
  static const repositorySearchUrl = 'search/repositories?q=flutter';
  static const issueSearchUrl = 'search/issues?q=flutter&page=1&per_page=10';
  static const commitSearchUrl= "https://api.github.com/search/commits?q=flutter&page=1&per_page=10";
  static const eventsUrl= "https://api.github.com/events";
  static const feedsUrl= "https://api.github.com/feeds";
  static const publicGistsUrl= "https://api.github.com/gists/public";
  static const rateLimitUrl = "https://api.github.com/rate_limit";

// These url need authention
  static const currentUserUrl= "https://api.github.com/user";
  static const currentUserAuthorizationsHtmlUrl= "https://github.com/settings/connections/applications{/client_id}";
  static const authorizationsUrl= "https://api.github.com/authorizations";
  static const codeSearchUrl= "https://api.github.com/search/code?q=flutter&page=1&per_page=10";
  static const emailsUrl= "https://api.github.com/user/emails";
  static const followersUrl= "https://api.github.com/user/followers";
  static const followingUrl= "https://api.github.com/user/following{/target}";
  static const gistsUrl= "https://api.github.com/gists{/gist_id}";
  static const hubUrl= "https://api.github.com/hub";
  static const issuesUrl= "https://api.github.com/issues";
  static const keysUrl= "https://api.github.com/user/keys";
  static const labelSearchUrl= "https://api.github.com/search/labels?q={query}&repository_id={repository_id}{&page,per_page}";
  static const notificationsUrl= "https://api.github.com/notifications";
  static const organizationUrl= "https://api.github.com/orgs/{org}";
  static const organizationRepositoriesUrl= "https://api.github.com/orgs/{org}/repos{?type,page,per_page,sort}";
  static const organizationTeamsUrl= "https://api.github.com/orgs/{org}/teams";
  static const repositoryUrl = "https://api.github.com/repos/{owner}/{repo}";
  static const currentUserRepositoriesUrl = "https://api.github.com/user/repos{?type,page,per_page,sort}";
  static const starredUrl = "https://api.github.com/user/starred{/owner}{/repo}";
  static const starredGistsUrl = "https://api.github.com/gists/starred";
  static const userOrganizationsUrl = "https://api.github.com/user/orgs";
  static const userRepositoriesUrl = "https://api.github.com/users/{user}/repos{?type,page,per_page,sort}";

}

