
class AssetsPath{
// Image assets path
  static const gitImage = 'assets/images/git.png';
  static const gitRepoImage = 'assets/images/gitRepo.png';
  static const fileNotFoundImage = 'assets/images/file_not_found.png';
//Icon assets path
  static const drawerIcon = 'assets/icons/drawer.png';
  static const filterIcon = 'assets/icons/filter.png';
}
