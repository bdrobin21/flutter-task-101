import 'package:flutter/material.dart';

class AppColor{
  static const kPrimaryColor = Color(0xff040D21);
  static const kPrimarySwatch = Colors.indigo;
  static const kAccentColor = Colors.cyan;
  static const kBlackColor = Colors.black;
  static const kWhiteColor = Colors.white;
  static const kGreyColor = Colors.grey;
  static const kWarningColor = Colors.amberAccent;
  static const kErrorColor = Colors.red;
  static const kEarthBackgroundColor = Color(0xff043E59);
  static const kElevetedButtonBackgroundColor = Color(0xff00C09A);
}
