

class OtherConstant{
  //Padding
  static const double kLargePadding = 16.0;
  static const double kRegularPadding = 8.0;
  static const double kSmallPadding = 4.0;

  //Radius
  static const double kLargeRadius = 30.0;
  static const double kRegularRadius = 10.0;
  static const double kSmallRadius = 5.0;

  //Text Size
  static const double kSmallTextSize = 12.0;
  static const double kRegularTextSize = 16.0;
  static const double kLargeTextSize = 20.0;
  static const double kHeadlineTextSize = 28.0;

  //Other size
  static const double kListItemImageHeightWidth = 100.0;
  static const double kDividerHeight = 30.0;
  static const double kIconSize = 30.0;
  static const int kDefaultListLength = 10;

  //Duration
  static const int kLockDurationInMinute = 30;

  //Status code
  static const int kSuccessCode = 200;
}