class RoutePath{
  static const home = '/homePage';
  static const login = '/loginPage';
  static const userListPage = '/userListPage';
  static const userDetailsPage = '/userDetailsPage';
}