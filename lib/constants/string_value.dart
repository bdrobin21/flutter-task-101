
class AppStaticString{
  static const appName='Flutter Task 101';
  static const welcome='Welcome';
  static const homePage='Home';
  static const loginPage='Login';
  static const getStarted='Get started';
  static const pullDownToRefresh='Pull down to refresh';
  static const updatedAt='Updated at : ';
  static const language='Language : ';

  //Text field hint
  static const usernameTextField = 'Username. Email or Mobile No';
  static const passwordTextField = 'Password';

  //Button name
  static const forgetButton = 'Forget password?';
  static const loginButton = 'Login';

  //Hive Box
  static const userListBox ='userListItemBox';
  static const repoListBox ='repoListItemBox';

  //Get Storage
  static const userListPageNumber = 'userListPageNumber';
  static const repoListPageNumber = 'repoListPageNumber';

  //Lock
  static const lock = 'Lock';
  static const unlock = 'Unlock';
  static const fetchUserListKey = 'fetchUserList';
  static const fetchRepoListKey = 'fetchRepoList';

  //Sorting
  static const mostRecent = 'Most recent';
  static const mostPopular = 'Most popular';

  //Exception
  static const socketException='Check your internet connection';
  static const timeoutException='Request timeout';
  static const noDataFound='No data found';
  static const notAvailable='Not available';
  static const functionNotImplemented='Function not implemented';
  static const validUserInfo='Enter valid info';
  static const validPassword='Enter valid password';
}