
import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/utils/helper/retry_request.dart';
import 'package:flutter_task_101/utils/request.dart';
import 'package:get/get.dart';

class AuthController extends GetxController implements RetryIfRequestFailed {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  RxBool passwordVisibility = RxBool(false);

  void login() async {
    String url = "Set login url here";
    Request request = Request(url: url);
    await request.get().then((response) {
      if (response!.statusCode == OtherConstant.kSuccessCode) {
        /// Do something if request success
      }
    }).catchError((e) {
      ///Do something if got error
    });
  }

  void passwordVisibilitySet(){
    passwordVisibility.value = !passwordVisibility.value;
    update();
  }

  @override
  void retryRequest() {
    login();
  }
}
