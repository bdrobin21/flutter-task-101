import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/api_path.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/features/model/repository_search_model.dart';
import 'package:flutter_task_101/utils/helper/lock_request.dart';
import 'package:flutter_task_101/utils/helper/retry_request.dart';
import 'package:flutter_task_101/utils/request.dart';
import 'package:flutter_task_101/utils/helper/time_out_delay.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive/hive.dart';
import 'package:flutter_task_101/utils/helper/sorting.dart';

class RepositorySearchController extends GetxController implements TimeOutDelayed,Sorting,RetryIfRequestFailed {
  final Box<Items> _repoListItemBox = Hive.box<Items>(AppStaticString.repoListBox);
  final RxList<Items> _repoListData = RxList();
  final RxList<Items> _getRepoList = RxList();
  final RxList<Items> generatedList = RxList();
  RxBool isLoading = RxBool(false);
  int? _pageNumber = 1;
  final RxInt _listLength = RxInt(OtherConstant.kDefaultListLength);
  final _box = GetStorage();
  final LockRequest _lockRequest = LockRequest();
  final ScrollController scrollController = ScrollController();

  @override
  void onInit() {
    super.onInit();
    _getRepoList.assignAll(_repoListItemBox.values);
    updatePaginetedList();
  }

  //For fetching data from internet
  Future<void> fetchRepositoryData() async {

    isLoading.value = true;
    _pageNumber = _box.read(AppStaticString.repoListPageNumber) == null
        ? 1
        : _box.read(AppStaticString.repoListPageNumber) + 1;
    String url = ApiPath.repositorySearchUrl +
        ApiPath.pageNo +
        _pageNumber.toString() +
        ApiPath.perPage;
    Request request = Request(url: url);
    if (!_lockRequest.isLocked(key: AppStaticString.fetchRepoListKey)) {
      _lockRequest.setLockState(key: AppStaticString.fetchRepoListKey);
      await request.get().then((response) {

        if (response!.statusCode == OtherConstant.kSuccessCode) {
          var repo = RepositorySearchModel.fromJson(jsonDecode(response.body));
          for (var element in repo.items!) {
            _repoListItemBox.add(element);
          }
          _getRepoList.assignAll(_repoListItemBox.values);
          _box.write(AppStaticString.repoListPageNumber, _pageNumber);
          generateList(listLength: _listLength.value, getList: _getRepoList);
          isLoading.value = false;
          unlockRequest();
          update();
        }
      }).catchError((e) {
        isLoading.value = false;
      });
    } else {
      isLoading.value = false;
    }
  }

  // For generating list
  void generateList({int? listLength = OtherConstant.kDefaultListLength, dynamic getList}) {
    generatedList
        .assignAll(List.generate(listLength!, (index) => getList[index]));
  }

  // For Update generated list
  void updatePaginetedList() async {
    if (_getRepoList.length > OtherConstant.kDefaultListLength) {
      generateList(getList: _getRepoList);
    }
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (_getRepoList.length.isGreaterThan(_listLength.value)) {
          _listLength.value = _listLength.value + OtherConstant.kDefaultListLength;
        }
        generateList(listLength: _listLength.value, getList: _getRepoList);
      }
    });
  }

  //To unlock for repeat call fetch request
  @override
  void unlockRequest() {
    Timer.periodic(const Duration(minutes:OtherConstant.kLockDurationInMinute), (timer) {
      _lockRequest.setUnlockState(key: AppStaticString.fetchRepoListKey);
      fetchRepositoryData();
    });
  }

  void onClear() {
    generatedList.clear();
    _getRepoList.clear();
    _repoListData.clear();
    _repoListItemBox.clear();
    _box.erase();
  }

  // Sorting list based on latest update time and repo popularity
 @override
  void mostPopular(){
    _getRepoList.sort((a, b) => b.updatedAt.toString().compareTo(a.updatedAt.toString()));
    generateList(listLength: _listLength.value, getList: _getRepoList);
  }

  @override
  void mostRecent() {
    _getRepoList.sort((a, b) => b.stargazersCount!.compareTo(a.stargazersCount!));
    generateList(listLength: _listLength.value, getList: _getRepoList);
  }

  // Retry request if api fetching failed
  @override
  void retryRequest() {
    _lockRequest.setUnlockState(key: AppStaticString.fetchRepoListKey);
        fetchRepositoryData();
  }
}


