import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/api_path.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/features/model/user_list_model.dart';
import 'package:flutter_task_101/utils/helper/lock_request.dart';
import 'package:flutter_task_101/utils/helper/retry_request.dart';
import 'package:flutter_task_101/utils/request.dart';
import 'package:flutter_task_101/utils/helper/time_out_delay.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive/hive.dart';
import 'package:flutter_task_101/constants/string_value.dart';



class UserController extends GetxController implements TimeOutDelayed,RetryIfRequestFailed{
  Box<Items> userListItemBox = Hive.box<Items>(AppStaticString.userListBox);
  final ScrollController scrollController = ScrollController();
  RxList<Items> userListData = RxList();
  RxList<Items> getUserList = RxList();
  final LockRequest _lockRequest = LockRequest();
  RxList<Items> generatedList = RxList();
  RxInt listLength = RxInt(OtherConstant.kDefaultListLength);

  final box = GetStorage();
  int _pageNumber = 1;

  RxBool isLoading = RxBool(false);
  @override
  void onInit() {
    super.onInit();
    // handleController();
    getUserList.assignAll(userListItemBox.values);
    updatePaginetedList();
  }

  // For Update generated list
 void updatePaginetedList() async {
    if (getUserList.length > OtherConstant.kDefaultListLength) {
      generateList(getList: getUserList);
    }
   scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (getUserList.length.isGreaterThan(listLength.value)) {
          listLength.value = listLength.value + OtherConstant.kDefaultListLength;
        }
        generateList(listLength:listLength.value,getList: getUserList);
      }
    });
  }

  // For generating list
 void generateList({int? listLength = OtherConstant.kDefaultListLength,dynamic getList}){
    generatedList.assignAll(List.generate(
        listLength!,
            (index) => getList[index])) ;
  }

  //For fetching data from internet
  Future<void> fetchUserList() async {
    _pageNumber = box.read(AppStaticString.userListPageNumber) == null
        ? 1
        : box.read(AppStaticString.userListPageNumber) + 1;

    Request request = Request(
        url: ApiPath.userSearchUrl +
            ApiPath.pageNo +
            _pageNumber.toString() +
            ApiPath.perPage);
    isLoading.value = true;
    if (!_lockRequest.isLocked(key: AppStaticString.fetchUserListKey)) {
      _lockRequest.setLockState(key: AppStaticString.fetchUserListKey);
      await request.get().then((response) {
        if (response!.statusCode == OtherConstant.kSuccessCode) {
          var decode = jsonDecode(response.body);
          var items = decode['items'];

          userListData.assignAll((items as List).map((e) {
            return Items(
                login: e['login'],
                id: e['id'],
                avatarUrl: e['avatar_url'],
                type: e['type'],
                dateTime: DateTime.now().toString());
          }).toList());

          for (var element in userListData) {
            userListItemBox.add(element);
          }

          getUserList.assignAll(userListItemBox.values);
          box.write(AppStaticString.userListPageNumber, _pageNumber);
          isLoading.value = false;
          generateList(listLength:listLength.value,getList: getUserList);
          unlockRequest();
          update();
        }
      }).catchError((e) {
        isLoading.value = false;
      });
    } else {
      isLoading.value = false;
      unlockRequest();
    }
  }

  //To unlock for repeat call fetch request
  @override
  void unlockRequest() {
    Timer.periodic(const Duration(minutes: OtherConstant.kLockDurationInMinute), (timer) {
      _lockRequest.setUnlockState(key: AppStaticString.fetchUserListKey);
      fetchUserList();
    });
  }
  //for clear save data
  void onClear() {
    userListItemBox.clear();
    getUserList.clear();
    generatedList.clear();
    box.erase();
    _lockRequest.eraseLockStatus(key: AppStaticString.fetchUserListKey);
  }

  @override
  void retryRequest() {
    _lockRequest.setUnlockState(key: AppStaticString.fetchUserListKey);
    fetchUserList();
  }


}


