import 'package:hive/hive.dart';
part 'repository_search_model.g.dart';

@HiveType(typeId: 5)
class RepositorySearchModel {@HiveField(0)
  int? totalCount;@HiveField(1)
  bool? incompleteResults;@HiveField(2)
  List<Items>? items;

  RepositorySearchModel({this.totalCount, this.incompleteResults, this.items});

  RepositorySearchModel.fromJson(Map<String, dynamic> json) {
    totalCount = json['total_count'];
    incompleteResults = json['incomplete_results'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_count'] = this.totalCount;
    data['incomplete_results'] = this.incompleteResults;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
@HiveType(typeId: 6)
class Items {@HiveField(0)
  int? id;@HiveField(1)
  String? nodeId;@HiveField(2)
  String? name;@HiveField(3)
  String? fullName;@HiveField(4)
  bool? private;@HiveField(5)
  Owner? owner;@HiveField(6)
  String? htmlUrl;@HiveField(7)
  String? description;@HiveField(8)
  bool? fork;@HiveField(9)
  String? url;@HiveField(10)
  String? forksUrl;@HiveField(11)
  String? keysUrl;@HiveField(12)
  String? collaboratorsUrl;@HiveField(13)
  String? teamsUrl;@HiveField(14)
  String? hooksUrl;@HiveField(15)
  String? issueEventsUrl;@HiveField(16)
  String? eventsUrl;@HiveField(17)
  String? assigneesUrl;@HiveField(18)
  String? branchesUrl;@HiveField(19)
  String? tagsUrl;@HiveField(20)
  String? blobsUrl;@HiveField(21)
  String? gitTagsUrl;@HiveField(22)
  String? gitRefsUrl;@HiveField(23)
  String? treesUrl;@HiveField(24)
  String? statusesUrl;@HiveField(25)
  String? languagesUrl;@HiveField(26)
  String? stargazersUrl;@HiveField(27)
  String? contributorsUrl;@HiveField(28)
  String? subscribersUrl;@HiveField(29)
  String? subscriptionUrl;@HiveField(30)
  String? commitsUrl;@HiveField(31)
  String? gitCommitsUrl;@HiveField(32)
  String? commentsUrl;@HiveField(33)
  String? issueCommentUrl;@HiveField(34)
  String? contentsUrl;@HiveField(35)
  String? compareUrl;@HiveField(36)
  String? mergesUrl;@HiveField(37)
  String? archiveUrl;@HiveField(38)
  String? downloadsUrl;@HiveField(39)
  String? issuesUrl;@HiveField(40)
  String? pullsUrl;@HiveField(41)
  String? milestonesUrl;@HiveField(42)
  String? notificationsUrl;@HiveField(43)
  String? labelsUrl;@HiveField(44)
  String? releasesUrl;@HiveField(45)
  String? deploymentsUrl;@HiveField(46)
  String? createdAt;@HiveField(47)
  String? updatedAt;@HiveField(48)
  String? pushedAt;@HiveField(49)
  String? gitUrl;@HiveField(50)
  String? sshUrl;@HiveField(51)
  String? cloneUrl;@HiveField(52)
  String? svnUrl;@HiveField(53)
  String? homepage;@HiveField(54)
  int? size;@HiveField(55)
  int? stargazersCount;@HiveField(56)
  int? watchersCount;@HiveField(57)
  String? language;@HiveField(58)
  bool? hasIssues;@HiveField(59)
  bool? hasProjects;@HiveField(60)
  bool? hasDownloads;@HiveField(61)
  bool? hasWiki;@HiveField(62)
  bool? hasPages;@HiveField(63)
  int? forksCount;@HiveField(64)
  Null? mirrorUrl;@HiveField(65)
  bool? archived;@HiveField(66)
  bool? disabled;@HiveField(67)
  int? openIssuesCount;@HiveField(68)
  License? license;@HiveField(69)
  bool? allowForking;@HiveField(70)
  bool? isTemplate;@HiveField(71)
  bool? webCommitSignoffRequired;@HiveField(72)
  List<String>? topics;@HiveField(73)
  String? visibility;@HiveField(74)
  int? forks;@HiveField(75)
  int? openIssues;@HiveField(76)
  int? watchers;@HiveField(77)
  String? defaultBranch;@HiveField(78)
  double? score;

  Items(
      {this.id,
        this.nodeId,
        this.name,
        this.fullName,
        this.private,
        this.owner,
        this.htmlUrl,
        this.description,
        this.fork,
        this.url,
        this.forksUrl,
        this.keysUrl,
        this.collaboratorsUrl,
        this.teamsUrl,
        this.hooksUrl,
        this.issueEventsUrl,
        this.eventsUrl,
        this.assigneesUrl,
        this.branchesUrl,
        this.tagsUrl,
        this.blobsUrl,
        this.gitTagsUrl,
        this.gitRefsUrl,
        this.treesUrl,
        this.statusesUrl,
        this.languagesUrl,
        this.stargazersUrl,
        this.contributorsUrl,
        this.subscribersUrl,
        this.subscriptionUrl,
        this.commitsUrl,
        this.gitCommitsUrl,
        this.commentsUrl,
        this.issueCommentUrl,
        this.contentsUrl,
        this.compareUrl,
        this.mergesUrl,
        this.archiveUrl,
        this.downloadsUrl,
        this.issuesUrl,
        this.pullsUrl,
        this.milestonesUrl,
        this.notificationsUrl,
        this.labelsUrl,
        this.releasesUrl,
        this.deploymentsUrl,
        this.createdAt,
        this.updatedAt,
        this.pushedAt,
        this.gitUrl,
        this.sshUrl,
        this.cloneUrl,
        this.svnUrl,
        this.homepage,
        this.size,
        this.stargazersCount,
        this.watchersCount,
        this.language,
        this.hasIssues,
        this.hasProjects,
        this.hasDownloads,
        this.hasWiki,
        this.hasPages,
        this.forksCount,
        this.mirrorUrl,
        this.archived,
        this.disabled,
        this.openIssuesCount,
        this.license,
        this.allowForking,
        this.isTemplate,
        this.webCommitSignoffRequired,
        this.topics,
        this.visibility,
        this.forks,
        this.openIssues,
        this.watchers,
        this.defaultBranch,
        this.score});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nodeId = json['node_id'];
    name = json['name'];
    fullName = json['full_name'];
    private = json['private'];
    owner = json['owner'] != null ? new Owner.fromJson(json['owner']) : null;
    htmlUrl = json['html_url'];
    description = json['description'];
    fork = json['fork'];
    url = json['url'];
    forksUrl = json['forks_url'];
    keysUrl = json['keys_url'];
    collaboratorsUrl = json['collaborators_url'];
    teamsUrl = json['teams_url'];
    hooksUrl = json['hooks_url'];
    issueEventsUrl = json['issue_events_url'];
    eventsUrl = json['events_url'];
    assigneesUrl = json['assignees_url'];
    branchesUrl = json['branches_url'];
    tagsUrl = json['tags_url'];
    blobsUrl = json['blobs_url'];
    gitTagsUrl = json['git_tags_url'];
    gitRefsUrl = json['git_refs_url'];
    treesUrl = json['trees_url'];
    statusesUrl = json['statuses_url'];
    languagesUrl = json['languages_url'];
    stargazersUrl = json['stargazers_url'];
    contributorsUrl = json['contributors_url'];
    subscribersUrl = json['subscribers_url'];
    subscriptionUrl = json['subscription_url'];
    commitsUrl = json['commits_url'];
    gitCommitsUrl = json['git_commits_url'];
    commentsUrl = json['comments_url'];
    issueCommentUrl = json['issue_comment_url'];
    contentsUrl = json['contents_url'];
    compareUrl = json['compare_url'];
    mergesUrl = json['merges_url'];
    archiveUrl = json['archive_url'];
    downloadsUrl = json['downloads_url'];
    issuesUrl = json['issues_url'];
    pullsUrl = json['pulls_url'];
    milestonesUrl = json['milestones_url'];
    notificationsUrl = json['notifications_url'];
    labelsUrl = json['labels_url'];
    releasesUrl = json['releases_url'];
    deploymentsUrl = json['deployments_url'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pushedAt = json['pushed_at'];
    gitUrl = json['git_url'];
    sshUrl = json['ssh_url'];
    cloneUrl = json['clone_url'];
    svnUrl = json['svn_url'];
    homepage = json['homepage'];
    size = json['size'];
    stargazersCount = json['stargazers_count'];
    watchersCount = json['watchers_count'];
    language = json['language'];
    hasIssues = json['has_issues'];
    hasProjects = json['has_projects'];
    hasDownloads = json['has_downloads'];
    hasWiki = json['has_wiki'];
    hasPages = json['has_pages'];
    forksCount = json['forks_count'];
    mirrorUrl = json['mirror_url'];
    archived = json['archived'];
    disabled = json['disabled'];
    openIssuesCount = json['open_issues_count'];
    license =
    json['license'] != null ? new License.fromJson(json['license']) : null;
    allowForking = json['allow_forking'];
    isTemplate = json['is_template'];
    webCommitSignoffRequired = json['web_commit_signoff_required'];
    topics = json['topics'].cast<String>();
    visibility = json['visibility'];
    forks = json['forks'];
    openIssues = json['open_issues'];
    watchers = json['watchers'];
    defaultBranch = json['default_branch'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['node_id'] = this.nodeId;
    data['name'] = this.name;
    data['full_name'] = this.fullName;
    data['private'] = this.private;
    if (this.owner != null) {
      data['owner'] = this.owner!.toJson();
    }
    data['html_url'] = this.htmlUrl;
    data['description'] = this.description;
    data['fork'] = this.fork;
    data['url'] = this.url;
    data['forks_url'] = this.forksUrl;
    data['keys_url'] = this.keysUrl;
    data['collaborators_url'] = this.collaboratorsUrl;
    data['teams_url'] = this.teamsUrl;
    data['hooks_url'] = this.hooksUrl;
    data['issue_events_url'] = this.issueEventsUrl;
    data['events_url'] = this.eventsUrl;
    data['assignees_url'] = this.assigneesUrl;
    data['branches_url'] = this.branchesUrl;
    data['tags_url'] = this.tagsUrl;
    data['blobs_url'] = this.blobsUrl;
    data['git_tags_url'] = this.gitTagsUrl;
    data['git_refs_url'] = this.gitRefsUrl;
    data['trees_url'] = this.treesUrl;
    data['statuses_url'] = this.statusesUrl;
    data['languages_url'] = this.languagesUrl;
    data['stargazers_url'] = this.stargazersUrl;
    data['contributors_url'] = this.contributorsUrl;
    data['subscribers_url'] = this.subscribersUrl;
    data['subscription_url'] = this.subscriptionUrl;
    data['commits_url'] = this.commitsUrl;
    data['git_commits_url'] = this.gitCommitsUrl;
    data['comments_url'] = this.commentsUrl;
    data['issue_comment_url'] = this.issueCommentUrl;
    data['contents_url'] = this.contentsUrl;
    data['compare_url'] = this.compareUrl;
    data['merges_url'] = this.mergesUrl;
    data['archive_url'] = this.archiveUrl;
    data['downloads_url'] = this.downloadsUrl;
    data['issues_url'] = this.issuesUrl;
    data['pulls_url'] = this.pullsUrl;
    data['milestones_url'] = this.milestonesUrl;
    data['notifications_url'] = this.notificationsUrl;
    data['labels_url'] = this.labelsUrl;
    data['releases_url'] = this.releasesUrl;
    data['deployments_url'] = this.deploymentsUrl;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['pushed_at'] = this.pushedAt;
    data['git_url'] = this.gitUrl;
    data['ssh_url'] = this.sshUrl;
    data['clone_url'] = this.cloneUrl;
    data['svn_url'] = this.svnUrl;
    data['homepage'] = this.homepage;
    data['size'] = this.size;
    data['stargazers_count'] = this.stargazersCount;
    data['watchers_count'] = this.watchersCount;
    data['language'] = this.language;
    data['has_issues'] = this.hasIssues;
    data['has_projects'] = this.hasProjects;
    data['has_downloads'] = this.hasDownloads;
    data['has_wiki'] = this.hasWiki;
    data['has_pages'] = this.hasPages;
    data['forks_count'] = this.forksCount;
    data['mirror_url'] = this.mirrorUrl;
    data['archived'] = this.archived;
    data['disabled'] = this.disabled;
    data['open_issues_count'] = this.openIssuesCount;
    if (this.license != null) {
      data['license'] = this.license!.toJson();
    }
    data['allow_forking'] = this.allowForking;
    data['is_template'] = this.isTemplate;
    data['web_commit_signoff_required'] = this.webCommitSignoffRequired;
    data['topics'] = this.topics;
    data['visibility'] = this.visibility;
    data['forks'] = this.forks;
    data['open_issues'] = this.openIssues;
    data['watchers'] = this.watchers;
    data['default_branch'] = this.defaultBranch;
    data['score'] = this.score;
    return data;
  }
}
@HiveType(typeId: 7)
class Owner {@HiveField(0)
  String? login;@HiveField(1)
  int? id;@HiveField(2)
  String? nodeId;@HiveField(3)
  String? avatarUrl;@HiveField(4)
  String? gravatarId;@HiveField(5)
  String? url;@HiveField(6)
  String? htmlUrl;@HiveField(7)
  String? followersUrl;@HiveField(8)
  String? followingUrl;@HiveField(9)
  String? gistsUrl;@HiveField(10)
  String? starredUrl;@HiveField(11)
  String? subscriptionsUrl;@HiveField(12)
  String? organizationsUrl;@HiveField(13)
  String? reposUrl;@HiveField(14)
  String? eventsUrl;@HiveField(15)
  String? receivedEventsUrl;@HiveField(16)
  String? type;@HiveField(17)
  bool? siteAdmin;

  Owner(
      {this.login,
        this.id,
        this.nodeId,
        this.avatarUrl,
        this.gravatarId,
        this.url,
        this.htmlUrl,
        this.followersUrl,
        this.followingUrl,
        this.gistsUrl,
        this.starredUrl,
        this.subscriptionsUrl,
        this.organizationsUrl,
        this.reposUrl,
        this.eventsUrl,
        this.receivedEventsUrl,
        this.type,
        this.siteAdmin});

  Owner.fromJson(Map<String, dynamic> json) {
    login = json['login'];
    id = json['id'];
    nodeId = json['node_id'];
    avatarUrl = json['avatar_url'];
    gravatarId = json['gravatar_id'];
    url = json['url'];
    htmlUrl = json['html_url'];
    followersUrl = json['followers_url'];
    followingUrl = json['following_url'];
    gistsUrl = json['gists_url'];
    starredUrl = json['starred_url'];
    subscriptionsUrl = json['subscriptions_url'];
    organizationsUrl = json['organizations_url'];
    reposUrl = json['repos_url'];
    eventsUrl = json['events_url'];
    receivedEventsUrl = json['received_events_url'];
    type = json['type'];
    siteAdmin = json['site_admin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this.login;
    data['id'] = this.id;
    data['node_id'] = this.nodeId;
    data['avatar_url'] = this.avatarUrl;
    data['gravatar_id'] = this.gravatarId;
    data['url'] = this.url;
    data['html_url'] = this.htmlUrl;
    data['followers_url'] = this.followersUrl;
    data['following_url'] = this.followingUrl;
    data['gists_url'] = this.gistsUrl;
    data['starred_url'] = this.starredUrl;
    data['subscriptions_url'] = this.subscriptionsUrl;
    data['organizations_url'] = this.organizationsUrl;
    data['repos_url'] = this.reposUrl;
    data['events_url'] = this.eventsUrl;
    data['received_events_url'] = this.receivedEventsUrl;
    data['type'] = this.type;
    data['site_admin'] = this.siteAdmin;
    return data;
  }
}
@HiveType(typeId: 8)
class License {
  String? key;@HiveField(0)
  String? name;@HiveField(1)
  String? spdxId;@HiveField(2)
  String? url;@HiveField(3)
  String? nodeId;

  License({this.key, this.name, this.spdxId, this.url, this.nodeId});

  License.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    name = json['name'];
    spdxId = json['spdx_id'];
    url = json['url'];
    nodeId = json['node_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    data['name'] = this.name;
    data['spdx_id'] = this.spdxId;
    data['url'] = this.url;
    data['node_id'] = this.nodeId;
    return data;
  }
}
