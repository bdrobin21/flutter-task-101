import 'package:hive/hive.dart';
part 'user_list_model.g.dart';
@HiveType(typeId: 3)
class UserListModel {@HiveField(0)
  int? totalCount;@HiveField(1)
  bool? incompleteResults;@HiveField(2)
  List<Items>? items;

  UserListModel({this.totalCount, this.incompleteResults, this.items});

  UserListModel.fromJson(Map<String, dynamic> json) {
    totalCount = json['total_count'];
    incompleteResults = json['incomplete_results'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_count'] = this.totalCount;
    data['incomplete_results'] = this.incompleteResults;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
@HiveType(typeId: 4)
class Items {@HiveField(0)
  String? login;@HiveField(1)
  int? id;@HiveField(2)
  String? avatarUrl;@HiveField(3)
  String? type;@HiveField(4)
  String? dateTime;

  Items({this.login, this.id, this.avatarUrl, this.type, this.dateTime});

  Items.fromJson(Map<String, dynamic> json) {
    login = json['login'];
    id = json['id'];
    avatarUrl = json['avatar_url'];
    type = json['type'];
    dateTime = json['dateTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this.login;
    data['id'] = this.id;
    data['avatar_url'] = this.avatarUrl;
    data['type'] = this.type;
    data['dateTime'] = this.dateTime;
    return data;
  }
}
