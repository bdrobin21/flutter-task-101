import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/colors.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/features/controller/repository_search_controller.dart';
import 'package:flutter_task_101/features/view/repository_details_page.dart';
import 'package:flutter_task_101/shared_component/custom_style.dart';
import 'package:flutter_task_101/shared_component/custom_widget.dart';
import 'package:flutter_task_101/utils/time_convert.dart';
import 'package:get/get.dart';
import 'login_page.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final RepositorySearchController _repositorySearchController =
      Get.put(RepositorySearchController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppStaticString.homePage),
        backgroundColor: AppColor.kEarthBackgroundColor,
        actions: [
          PopupMenuButton(
              icon: const Icon(Icons.filter_alt),
              itemBuilder: (context) => [

                    PopupMenuItem(
                      child: const Text(AppStaticString.mostRecent),
                      value: AppStaticString.mostRecent,
                      onTap: () => _repositorySearchController.mostPopular(),
                    ),
                    PopupMenuItem(
                      child: const Text(AppStaticString.mostPopular),
                      value:AppStaticString.mostPopular,
                      onTap: () => _repositorySearchController.mostRecent(),
                    ),

                  ]),
          IconButton(onPressed: (){
            Get.to(()=> LoginPage());
          }, icon: const Icon(Icons.account_circle_outlined)),

        ],
      ),
      body: SafeArea(
        child: GetX<RepositorySearchController>(
            init: RepositorySearchController(),
            initState: (_) {
              _.controller!.fetchRepositoryData();
            },
            builder: (_) {

              return _.generatedList.isNotEmpty
                  ? ListView.builder(
                      itemCount: _.generatedList.length,
                      controller: _.scrollController,
                      shrinkWrap: true,
                      primary: false,
                      itemBuilder: (context, index) {
                        String name = _.generatedList[index].name?? AppStaticString.notAvailable;
                        String description = _.generatedList[index].description ?? AppStaticString.notAvailable;
                        String lastUpdate = _.generatedList[index].updatedAt != null? TimeConverts.toLongDateTime(_.generatedList[index].updatedAt.toString()):AppStaticString.notAvailable;
                        String language = _.generatedList[index].language ??AppStaticString.notAvailable;
                        int star=  _repositorySearchController.generatedList[index].stargazersCount ?? 0;
                        return GestureDetector(
                          onTap: (){Get.to(()=>RepositoryDetailsPage(index: index));},
                          child: Container(
                            margin: const EdgeInsets.symmetric(vertical: OtherConstant.kSmallPadding, horizontal: OtherConstant.kLargePadding),
                            padding: const EdgeInsets.all(OtherConstant.kRegularPadding),
                            decoration: CustomStyle.kBoxDecoration(),
                            child: CustomWidget.repoListItem(
                               name : name,
                             description : description,
                             lastUpdate : lastUpdate,
                            language : language,
                              star:  star,
                            ),
                          ),
                        );
                      })
                  : _.isLoading.value
                      ? const Center(child: CircularProgressIndicator())
                      : RefreshIndicator(
                        onRefresh: ()async {
                          _.retryRequest();
                        },
                        child: ListView(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(OtherConstant.kLargePadding),
                              child: Text(AppStaticString.pullDownToRefresh,textAlign: TextAlign.center,style: CustomStyle.kDefaultTextStyle(color: AppColor.kGreyColor),),
                            ),
                            CustomWidget.noDataFound(context),
                          ],
                        ),
                      );
            },

        ),
      ),
    );
  }
}
