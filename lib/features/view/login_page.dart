import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/assets_path.dart';
import 'package:flutter_task_101/constants/colors.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/features/controller/auth_controller.dart';
import 'package:flutter_task_101/shared_component/custom_style.dart';
import 'package:flutter_task_101/shared_component/custom_widget.dart';
import 'package:flutter_task_101/utils/custom_exception_snackbar.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final AuthController _authController = Get.put(AuthController());
  final _formKey = GlobalKey<FormState>();
  bool passwordVisibility = false;

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.kWhiteColor,
      appBar: AppBar(backgroundColor: AppColor.kEarthBackgroundColor,elevation: 0,),
      body: SafeArea(
        child:Padding(
          padding: const EdgeInsets.all(OtherConstant.kLargePadding),
          child: Stack(children: [
            Align(alignment: Alignment.bottomRight,child: Image.asset(AssetsPath.gitImage,width:Get.size.width * .5,)),
            SingleChildScrollView(child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: OtherConstant.kRegularPadding),
                    child: Text(AppStaticString.loginPage,style: CustomStyle.kHeadlineTextStyle(color: AppColor.kEarthBackgroundColor,size: OtherConstant.kHeadlineTextSize),),
                  ),
                  CustomWidget.kCustomTextField(hint: AppStaticString.usernameTextField,
                      validator:(value)=> value.isEmpty?AppStaticString.validUserInfo:null,
                    controller: _authController.usernameController
                  ),
                   Obx(() => CustomWidget.kCustomTextField(hint: AppStaticString.passwordTextField,
                       validator:(value)=> value.isEmpty || value.toString().length < 6 ?AppStaticString.validPassword:null,
                       controller: _authController.passwordController,
                       obscureText: !_authController.passwordVisibility.value,
                       suffixIcon: IconButton(onPressed: (){
                         _authController.passwordVisibilitySet();
                       }, icon:_authController.passwordVisibility.value ?const Icon(Icons.visibility): const Icon(Icons.visibility_off) )
                   ),),

                  TextButton(onPressed: (){
                    CustomSnackbar.showSnackbar(
                        message: AppStaticString.functionNotImplemented);
                  }, child: const Text(AppStaticString.forgetButton)),
                   SizedBox(width: Get.size.width,child: CustomWidget.kCustomElevatedButton(onPressed: (){
                     if(_formKey.currentState!.validate()){
                      // _authController.login();
                       CustomSnackbar.showSnackbar(
                           message: AppStaticString.functionNotImplemented);
                     }
                   },title: AppStaticString.loginButton,),)
                        ],
              ),
            )),
          ],),
        ),
      ),
    );
  }

}
