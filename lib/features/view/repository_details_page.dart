import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/colors.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/features/controller/repository_search_controller.dart';
import 'package:flutter_task_101/shared_component/custom_style.dart';
import 'package:flutter_task_101/shared_component/custom_widget.dart';
import 'package:flutter_task_101/utils/custom_exception_snackbar.dart';
import 'package:flutter_task_101/utils/time_convert.dart';
import 'package:get/get.dart';

class RepositoryDetailsPage extends StatefulWidget {
  // final Items items;
  final int index;
  const RepositoryDetailsPage({Key? key, required this.index})
      : super(key: key);

  @override
  State<RepositoryDetailsPage> createState() => _RepositoryDetailsPageState();
}

class _RepositoryDetailsPageState extends State<RepositoryDetailsPage> {
  final RepositorySearchController _repositorySearchController =
  Get.put(RepositorySearchController());

  @override
  Widget build(BuildContext context) {
    var description = _repositorySearchController.generatedList[widget.index].description ?? AppStaticString.notAvailable;
    var ownerName = _repositorySearchController.generatedList[widget.index].owner!.login ?? AppStaticString.notAvailable;
    var repoName = _repositorySearchController.generatedList[widget.index].name ?? AppStaticString.notAvailable;
    var star = _repositorySearchController.generatedList[widget.index].stargazersCount ?? 0;
    var language = _repositorySearchController.generatedList[widget.index].language ?? AppStaticString.notAvailable;
    var updatedAt = _repositorySearchController.generatedList[widget.index].updatedAt != null
        ? TimeConverts.toLongDateTime(_repositorySearchController.generatedList[widget.index].updatedAt.toString())
        : AppStaticString.notAvailable;
   return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SizedBox(
              height: Get.height * 0.5,
              child: CustomWidget.kCachedNetworkImage(
                url: _repositorySearchController.generatedList[widget.index].owner?.avatarUrl,
                height: Get.height * 0.5,
                width: Get.width,
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: Get.height * 0.6,
                width: Get.size.width,
                padding: const EdgeInsets.all(OtherConstant.kLargePadding),
                decoration: CustomStyle.kBoxDecoration(
                    color: AppColor.kWhiteColor,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(OtherConstant.kLargeRadius),
                        topRight: Radius.circular(OtherConstant.kLargeRadius))),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: Text(
                           ownerName,
                            style: CustomStyle.kHeadlineTextStyle( size: OtherConstant.kHeadlineTextSize),
                          )),
                          IconButton(
                            icon: const Icon(Icons.share_outlined),
                            onPressed: () {
                              CustomSnackbar.showSnackbar(
                                  message: AppStaticString.functionNotImplemented);
                            },
                          )
                        ],
                      ),
              CustomWidget.repoListItem(
                name : repoName,
                lastUpdate : updatedAt,
                language : language,
                star:  star,
              ),

                      const Divider(
                        height: OtherConstant.kDividerHeight,
                        color: AppColor.kElevetedButtonBackgroundColor,
                      ),

                      Wrap(
                          children: _repositorySearchController.generatedList[widget.index].topics!
                              .map((e) => Container(
                            margin: const EdgeInsets.all(OtherConstant.kSmallPadding),
                            padding: const EdgeInsets.all(OtherConstant.kRegularPadding),
                                    decoration: CustomStyle.kBoxDecoration(
                                        color: AppColor
                                            .kElevetedButtonBackgroundColor),
                                    child: Text(e,style: CustomStyle.kDefaultTextStyle(color: AppColor.kWhiteColor),),
                                  ))
                              .toList()),
                      Padding(
                        padding: const EdgeInsets.only(top: OtherConstant.kRegularPadding),
                        child: Text(
                          description,
                          textAlign: TextAlign.justify,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: const Icon(
                  Icons.arrow_back_ios,
                  size:OtherConstant.kIconSize
                ))
          ],
        ),
      ),
    );
  }
}
