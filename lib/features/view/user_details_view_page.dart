import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/colors.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/features/controller/user_controller.dart';
import 'package:flutter_task_101/shared_component/custom_style.dart';
import 'package:flutter_task_101/shared_component/custom_widget.dart';
import 'package:flutter_task_101/utils/custom_exception_snackbar.dart';
import 'package:get/get.dart';

class UserDetailsPage extends StatefulWidget {
  final int index;
  const UserDetailsPage({Key? key,required this.index}) : super(key: key);

  @override
  State<UserDetailsPage> createState() => _UserDetailsPageState();
}

class _UserDetailsPageState extends State<UserDetailsPage> {
  final UserController _userController = Get.put(UserController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SizedBox(
              height: Get.height * 0.5,
              child: CustomWidget.kCachedNetworkImage(
                url: _userController.generatedList[widget.index].avatarUrl,
                height: Get.height * 0.5,
                width: Get.width,
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: Get.height * 0.6,
                width: Get.size.width,
                padding: const EdgeInsets.all(OtherConstant.kLargePadding),
                decoration: CustomStyle.kBoxDecoration(
                    color: AppColor.kWhiteColor,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(OtherConstant.kLargeRadius),
                        topRight: Radius.circular(OtherConstant.kLargeRadius))),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: Text(
                                _userController.generatedList[widget.index].login!,
                                style: CustomStyle.kHeadlineTextStyle( size: OtherConstant.kHeadlineTextSize),
                              )),
                          IconButton(
                            icon: const Icon(Icons.share_outlined),
                            onPressed: () {
                              CustomSnackbar.showSnackbar(
                                  message: AppStaticString.functionNotImplemented);
                            },
                          )
                        ],
                      ),


                      const Divider(
                        height: OtherConstant.kDividerHeight,
                        color: AppColor.kElevetedButtonBackgroundColor,
                      ),

                    ],
                  ),
                ),
              ),
            ),
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: const Icon(
                    Icons.arrow_back_ios,
                    size:OtherConstant.kIconSize
                ))
          ],
        ),
      ),
    );
  }
}
