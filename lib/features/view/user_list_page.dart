import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/colors.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/features/controller/user_controller.dart';
import 'package:flutter_task_101/features/view/user_details_view_page.dart';
import 'package:flutter_task_101/shared_component/custom_style.dart';
import 'package:flutter_task_101/shared_component/custom_widget.dart';
import 'package:get/get.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({Key? key}) : super(key: key);

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  final UserController _userController = Get.put(UserController());
  @override
  void initState() {
    super.initState();
    _userController.fetchUserList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GetX<UserController>(
          init: UserController(),
          initState: (_) {
            _.controller!.fetchUserList();
          },
          builder: (_) {
            return _.generatedList.isNotEmpty
                ? ListView.builder(
                itemCount: _.generatedList.length,
                controller: _.scrollController,
                shrinkWrap: true,
                primary: false,
                itemBuilder: (context, index) {
                  String name = _.generatedList[index].login?? AppStaticString.notAvailable;
                  String description = AppStaticString.notAvailable;
                  String lastUpdate = AppStaticString.notAvailable;
                  String language = AppStaticString.notAvailable;
                  int star= 0;
                  return GestureDetector(
                    onTap: (){Get.to(()=>UserDetailsPage(index: index));},
                    child: Container(
                      margin: const EdgeInsets.symmetric(vertical: OtherConstant.kSmallPadding, horizontal: OtherConstant.kLargePadding),
                      padding: const EdgeInsets.all(OtherConstant.kRegularPadding),
                      decoration: CustomStyle.kBoxDecoration(),
                      child: CustomWidget.repoListItem(
                        name : name,
                        description : description,
                        lastUpdate : lastUpdate,
                        language : language,
                        star:  star,
                      ),
                    ),
                  );
                })
                : _.isLoading.value
                ? const Center(child: CircularProgressIndicator())
                : RefreshIndicator(
              onRefresh: ()async {
                _.retryRequest();
              },
              child: ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(OtherConstant.kLargePadding),
                    child: Text(AppStaticString.pullDownToRefresh,textAlign: TextAlign.center,style: CustomStyle.kDefaultTextStyle(color: AppColor.kGreyColor),),
                  ),
                  CustomWidget.noDataFound(context),
                ],
              ),
            );
          },

        ),
      ),
    );
  }
}
