import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/colors.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/features/model/user_list_model.dart';
import 'package:flutter_task_101/utils/route.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'features/view/home_page.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_task_101/features/model/repository_search_model.dart' as repo;


Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  await GetStorage.init();
  Hive.registerAdapter(UserListModelAdapter());
  Hive.registerAdapter(ItemsAdapter());
  Hive.registerAdapter(repo.ItemsAdapter());
  Hive.registerAdapter(repo.OwnerAdapter());
  Hive.registerAdapter(repo.RepositorySearchModelAdapter());
  Hive.registerAdapter(repo.LicenseAdapter());
  await Hive.openBox<Items>(AppStaticString.userListBox);
  await Hive.openBox<repo.Items>(AppStaticString.repoListBox);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: AppStaticString.appName,
      getPages: Pages.route,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: AppColor.kPrimarySwatch,
      ),
      home: MyHomePage(),
    );
  }
}


