import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/colors.dart';
import 'package:flutter_task_101/constants/other_constant.dart';

class CustomStyle{
 static TextStyle kHeadlineTextStyle({double size = OtherConstant.kLargeTextSize,Color color = AppColor.kBlackColor,FontWeight fontWeight = FontWeight.w600}) => TextStyle(color: color,fontSize: size,fontWeight: fontWeight);
 static TextStyle kDefaultTextStyle({double size = OtherConstant.kRegularTextSize,Color color = AppColor.kBlackColor,FontWeight fontWeight = FontWeight.w400}) => TextStyle(color: color,fontSize: size,fontWeight: fontWeight);
 static ButtonStyle kCustomElevetedButtonStyle() => ButtonStyle(backgroundColor: MaterialStateProperty.all(AppColor.kElevetedButtonBackgroundColor));
 static BoxDecoration kBoxDecoration({Color color = AppColor.kWhiteColor, borderRadius})=> BoxDecoration(color: color,borderRadius: borderRadius??BorderRadius.circular(OtherConstant.kRegularRadius));
}