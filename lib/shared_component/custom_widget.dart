import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/assets_path.dart';
import 'package:flutter_task_101/constants/colors.dart';
import 'package:flutter_task_101/constants/other_constant.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/shared_component/custom_style.dart';
import 'package:flutter_task_101/utils/count_convert.dart';

class CustomWidget{
  static CachedNetworkImage kCachedNetworkImage(
      {required url,
        required double height,
        double? width,
        double borderRadius = 0}) {
    return CachedNetworkImage(
      imageUrl: url,
      imageBuilder: (context, imageProvider) => Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(borderRadius),
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) =>
      const Center(child: CircularProgressIndicator()),
      errorWidget: (context, url, error) =>
      const Center(child: Icon(Icons.error)),
    );
  }
  static Row repoListItem(
      {String? name,String? description,String? lastUpdate,String? language,int? star,String? photoUrl}) {
     return Row(
       crossAxisAlignment: CrossAxisAlignment.center,
       children: [
        photoUrl == null?
        Image.asset(AssetsPath.gitRepoImage,height: OtherConstant.kListItemImageHeightWidth,):
    SizedBox(
        height: OtherConstant.kListItemImageHeightWidth,
        width: OtherConstant.kListItemImageHeightWidth,
    child: CustomWidget.kCachedNetworkImage(
    url: photoUrl,
    height: OtherConstant.kListItemImageHeightWidth,
    width: OtherConstant.kListItemImageHeightWidth,
    borderRadius: OtherConstant.kSmallRadius)),
         Expanded(
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
                 listItemTextView(text: name,
                   textStyle: CustomStyle.kDefaultTextStyle(
                   fontWeight: FontWeight.w500,
                   ),
                 padding: const EdgeInsets.only(left: OtherConstant.kRegularPadding)
                 ),
               description != null? listItemTextView(text: description,textColor: AppColor.kGreyColor):const SizedBox(),
                 listItemTextView(text: AppStaticString.updatedAt+lastUpdate!,),

               Row(
                 children: [
                   listItemTextView(text: '☆ ${CountConvert.toThousand(star!)}'),
                   Expanded(child: listItemTextView(text:AppStaticString.language + language!,textColor:AppColor.kElevetedButtonBackgroundColor )),
                 ],
               ),
             ],
           ),
         ),
       ],
     );
  }

  static Padding listItemTextView({
  String? text,
    EdgeInsets? padding,
    Color textColor = AppColor.kBlackColor,
   TextStyle? textStyle
  }) {
    return Padding(
                        padding:padding ?? const EdgeInsets.only(left: OtherConstant.kRegularPadding, top: OtherConstant.kSmallPadding),
                        child: Text(
                          text!,
                          overflow: TextOverflow.ellipsis,
                          style: textStyle ?? CustomStyle.kDefaultTextStyle(color: textColor,size: OtherConstant.kSmallTextSize),
                        ),
                     );
  }

  static Container noDataFound(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      padding: const EdgeInsets.all(OtherConstant.kRegularPadding),
      margin: const EdgeInsets.all(OtherConstant.kLargePadding),
      decoration: CustomStyle.kBoxDecoration(),
      child: Column(
        children: [
          Image.asset(AssetsPath.fileNotFoundImage),
          Padding(
            padding: const EdgeInsets.all(OtherConstant.kRegularPadding),
            child: Text(
              AppStaticString.noDataFound,
              style: CustomStyle.kHeadlineTextStyle(fontWeight: FontWeight.w500),
            ),
          ),
        ],
      ),
    );
  }

  static kCustomTextField({hint,obscureText = false,suffixIcon,validator,controller}) => Padding(
      padding: const EdgeInsets.symmetric(vertical: OtherConstant.kRegularPadding,),
      child: TextFormField(obscureText: obscureText,
        validator: validator,
        controller: controller,
        decoration: InputDecoration(contentPadding: const EdgeInsets.symmetric(horizontal: OtherConstant.kLargePadding),hintText: hint,border: OutlineInputBorder(borderRadius: BorderRadius.circular(OtherConstant.kSmallRadius)),suffixIcon:suffixIcon?? const SizedBox()),
      ));

 static ElevatedButton kCustomElevatedButton({onPressed,title,backgroundColor,padding}) => ElevatedButton(onPressed: onPressed, child: Text(title??AppStaticString.notAvailable),style: ButtonStyle(backgroundColor: MaterialStateProperty.all(backgroundColor??AppColor.kEarthBackgroundColor),padding: MaterialStateProperty.all(padding??const EdgeInsets.all(OtherConstant.kLargePadding))),);

}



