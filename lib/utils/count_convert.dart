class CountConvert{
  static String toThousand(int number) {
    String a = '';
    if (number == null || number == "") return ' ';
    if(number > 999) {
      var calculated =  number / 1000 ;
      a = calculated.toInt().toString() + 'k';
    }else{
      a = number.toString();
    }

    return a;
  }
}