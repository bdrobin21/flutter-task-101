
import 'package:get/get.dart';

class CustomSnackbar{
 static void showSnackbar({message}){
   Get.showSnackbar(GetSnackBar(message:message,duration: const Duration(seconds: 1),));
  }


}