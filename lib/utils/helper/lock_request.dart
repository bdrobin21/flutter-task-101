import 'package:flutter_task_101/constants/string_value.dart';
import 'package:get_storage/get_storage.dart';

class LockRequest {
  final _box = GetStorage();
  void setLockState({String? key}) {
    _box.write(key!, AppStaticString.lock);
  }

  void setUnlockState({String? key}) {
    _box.write(key!, AppStaticString.unlock);
  }

  String? getLockStatus({String? key}) {
    return _box.read(key!);
  }

  void eraseLockStatus({String? key}) {
    _box.remove(key!);
  }

  bool isLocked({String? key}) {
    bool status = _box.read(key!) == AppStaticString.lock ? true : false;
    return status;
  }
}
