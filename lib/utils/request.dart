import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_task_101/constants/api_path.dart';
import 'package:flutter_task_101/constants/string_value.dart';
import 'package:flutter_task_101/utils/custom_exception_snackbar.dart';
import 'package:http/http.dart' as http;

class Request {
  final String url;
  final dynamic body;

  const Request({Key? key, required this.url, this.body});

  Future<http.Response?> get() async {
    try {
      return await http.get(Uri.parse(ApiPath.baseUrl + url), headers: {
        HttpHeaders.contentTypeHeader: 'application/json'
      }).timeout(const Duration(minutes: 2));
    } on SocketException {
      CustomSnackbar.showSnackbar(message: AppStaticString.socketException);
      return null;
    } on TimeoutException {
     CustomSnackbar.showSnackbar(message: AppStaticString.timeoutException);
      return null;
    }
  }

  Future<http.Response?> post() async {
    try {
      return await http.post(Uri.parse(ApiPath.baseUrl + url),
          body: body,
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json'
          }).timeout(const Duration(minutes: 2));
    } on SocketException {
      CustomSnackbar.showSnackbar(message: AppStaticString.socketException);
      return null;
    } on TimeoutException {
      CustomSnackbar.showSnackbar(message: AppStaticString.timeoutException);
      return null;
    }
  }
}
