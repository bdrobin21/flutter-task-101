import 'package:flutter_task_101/constants/route_path.dart';
import 'package:flutter_task_101/features/view/home_page.dart';
import 'package:flutter_task_101/features/view/user_list_page.dart';
import 'package:get/get.dart';

class Pages{
  static final route =[
    GetPage(name: RoutePath.home, page: ()=>MyHomePage()),
    GetPage(name: RoutePath.login, page: ()=>MyHomePage()),
    GetPage(name: RoutePath.userListPage, page: ()=>const UserListPage()),
    GetPage(name: RoutePath.userDetailsPage, page: ()=>MyHomePage()),
  ];
}