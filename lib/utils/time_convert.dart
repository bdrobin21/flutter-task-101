import 'package:intl/intl.dart';

class TimeConverts {
  static String toLongDateTime(String dt) {
    if (dt == null || dt == "") return ' ';
    var newFormat = DateFormat("MM-dd-yyyy").add_jm();
    return newFormat.format(DateTime.parse(dt));
  }
}